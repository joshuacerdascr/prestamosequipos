/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.PrestamosEquipos;
import javax.swing.JOptionPane;
/**
 *
 * @author familiacr
 */
public final class Menu {
    
    Equipo registrarEquipo;
    Usuario datosUsuario;
    public Menu(){
    crearMenu();
    }//fin de constructor
    public void crearMenu(){
   
    int opcion=0;
    
    while(opcion!=5){
     opcion=Integer.parseInt(JOptionPane.showInputDialog("""
                                                         Seleccione:                                                        
                                                         1.Solicitud de equipo
                                                         2.Mostrar datos del solicitante y el equipo solicitado
                                                         3.Salir
                                                         """));
     
     switch(opcion){ 
//solicitud de equipo         
         case 1:
          String identificacion=JOptionPane.showInputDialog("Digite el número de cédula del usuario: ");
          String nombre=JOptionPane.showInputDialog("Digite el nombre del usuario: ");
          String tipoUsuario=JOptionPane.showInputDialog("Digite si el usuario es estudiante o docente: ");
          String fechaPrestamo=JOptionPane.showInputDialog("Digite la fecha del préstamo del equipo: ");
          String fechaDevolucion=JOptionPane.showInputDialog("Digite la fecha de devolución del equipo: ");
          
            datosUsuario=new Usuario(identificacion,nombre,tipoUsuario,fechaPrestamo,fechaDevolucion);
            
          String tipoDeEquipo=JOptionPane.showInputDialog("Digite el tipo de equipo solicitado: ");
          String modelo=JOptionPane.showInputDialog("Digite el modelo de el equipo: ");
          String accesorios=JOptionPane.showInputDialog("Digite que accesorios posee el equipo: ");
          String estadoDelEquipo=JOptionPane.showInputDialog("Digite en que estado se encuentra el equipo(Observaciones): ");
          String placa=JOptionPane.showInputDialog("Digite la placa del equipo: ");
                
             registrarEquipo=new Equipo(tipoDeEquipo,modelo,accesorios,estadoDelEquipo,placa);  
             break;
//mostrar datos del solicitante y del equipo solicitado                         
         case 2:
             if(datosUsuario!=null & registrarEquipo!=null){
             JOptionPane.showMessageDialog(null,datosUsuario.toString()+registrarEquipo.toString());
             }//fin del if
             else{
              JOptionPane.showMessageDialog(null,"No se ha realizado solicitud de equipo");
             }//fin del else
             break;
      //Salir      
         case 3:
             System.exit(0);
             break;
             
         default:
             JOptionPane.showMessageDialog(null,"Digite una opción válida");
             break;
     
     }//fin del switch
    }//fin del while
    }//fin método Menu
}//fin de la clase Menu
