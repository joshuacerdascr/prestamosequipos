/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.PrestamosEquipos;

/**
 *
 * @author familiacr
 */
public class Usuario {
    private String identificacion, nombre, tipoUsuario, fechaPrestamo, fechaDevolucion;

    public Usuario() {
    }//fin constructor sin parámetros

    public Usuario(String identificacion, String nombre, String tipoUsuario, String fechaPrestamo, String fechaDevolucion) {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.tipoUsuario = tipoUsuario;
        this.fechaPrestamo = fechaPrestamo;
        this.fechaDevolucion = fechaDevolucion;    
    }//fin constructor con parámetros

    //sección de set y get
    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getFechaPrestamo() {
        return fechaPrestamo;
    }

    public void setFechaPrestamo(String fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }

    public String getFechaDevolucion() {
        return fechaDevolucion;
    }

    public void setFechaDevolucion(String fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    //fin de sección de get y set

    @Override
    public String toString() {
        return "\nUsuario:"
                +"\nIdentificacion: "+identificacion 
                + "\nNombre: "+nombre 
                + "\nTipo de usuario: "+tipoUsuario 
                + "\nFecha de préstamo: "+fechaPrestamo
                + "\nFecha de devolución: "+fechaDevolucion; 
                 
    }//fin toString
    
}//fin clase Usuario
