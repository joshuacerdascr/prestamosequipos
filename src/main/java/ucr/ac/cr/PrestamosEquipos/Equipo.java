/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.PrestamosEquipos;

/**
 *
 * @author familiacr
 */
public class Equipo {

    String tipoDeEquipo;
    private String modelo, accesorios, estadoDelEquipo, placa;

    public Equipo() {
    }//fin constructor sin parámetros

    public Equipo(String tipoDeEquipo, String modelo, String accesorios, String estadoDelEquipo, String placa) {
        this.tipoDeEquipo = tipoDeEquipo;
        this.modelo = modelo;
        this.accesorios = accesorios;
        this.estadoDelEquipo = estadoDelEquipo;
        this.placa = placa;
    }//fin constructor con parámetros

    //sección set y get
    public String getTipoDeEquipo() {
        return tipoDeEquipo;
    }

    public void setTipoDeEquipo(String tipoDeEquipo) {
        this.tipoDeEquipo = tipoDeEquipo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getAccesorios() {
        return accesorios;
    }

    public void setAccesorios(String accesorios) {
        this.accesorios = accesorios;
    }

    public String getEstadoDelEquipo() {
        return estadoDelEquipo;
    }

    public void setEstadoDelEquipo(String estadoDelEquipo) {
        this.estadoDelEquipo = estadoDelEquipo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }
    //fin constructor con parámetros

    
    @Override
    public String toString() {
        return  "\nEquipo:"
                +"\nTipo de equipo: "+tipoDeEquipo 
                + "\nModelo: "+modelo
                + "\nAccesorios: "+accesorios 
                + "\nEstado del equipo: "+estadoDelEquipo
                + "\nPlaca: "+placa; 
                
    }//fin de método toString
    
}//fin clase Equipo
